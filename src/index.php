<!DOCTYPE html>
<html lang="en">
	<head>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css"></link>
		<link rel="stylesheet" type="text/css" href="bootstrap-theme.css"></link>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2">
					<div class="panel panel-default">
						<div class="panel-heading">Pond Data</div>
						<table class="table table-hover">
							<tbody class="">
								<tr>
									<td>Total Frogs</td>
									<td id="total">0</td>
								</tr>
								<tr>
									<td>Male</td>
									<td id="male">0</td>
								</tr>
								<tr>
									<td>Female</td>
									<td id="female">0</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-md-8">
					<div class="panel panel-default">
						<div class="panel-heading">Pond  (click the name below the view the detail)</div>
						<div class="panel-body" id="pond"></div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="panel panel-default">
						<div class="panel-heading">Add frog</div>
						<div class="panel-body">
							<form id="add_form">
								<div class="form-group">
									<label>Name</label>
									<input id="name" id="name" type="text" class="form-control" placeholder="Name">
								</div>
								<div class="form-group">
									<label>Gender</label>
									<select name="gender" id="gender" class="form-control">
										<option value="male">Male</option>
										<option value="female">Female</option>
									</select>
								</div>
								<div class="form-group">
									<button class="btn btn-default" type="submit">Add</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2" id="frog_detail">
				</div>
				<div class="col-md-4" id="action">
				</div>
			</div>
		</div>
	</body>
</html>
<script src="js/jquery-1.11.2.min.js"></script>
<script src="js/bootstrap.js"></script> 
<script src="js/general.js"></script> 