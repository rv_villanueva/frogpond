$( document ).ready(function() {
    get_pond();
    get_pond_data();
});

function get_pond(){
	$.ajax({
		url: "ajax.php",
		type: "post",
		data: 'method=get_pond',
		dataType: 'html',
		success: function(response){
			$("#pond").html(response);
		}
	});
}

function mate(id){
	var mate_with = $('#mate_with').val();
	$.ajax({
		url: "ajax.php",
		type: "post",
		data: 'method=mate&frog_1='+id+'&frog_2='+mate_with,
		dataType: 'html',
		success: function(response){
			get_pond();
			get_pond_data();
			alert(response);
		}
	});
}

function remove_from_pond(id){
	$.ajax({
		url: "ajax.php",
		type: "post",
		data: 'method=remove_from_pond&frog_id='+id,
		dataType: 'html',
		success: function(response){
			get_pond();
			get_pond_data();
			alert(response);
		}
	});
}

function get_pond_data(){
	$.ajax({
		url: "ajax.php",
		type: "post",
		data: 'method=get_pond_data',
		dataType: 'json',
		success: function(response){
			$("#total").html(response.total);
			$("#female").html(response.female);
			$("#male").html(response.male);
		}
	});
}

function get_frog_data(id){
	$.ajax({
		url: "ajax.php",
		type: "post",
		data: 'method=get_frog_data&id='+id,
		dataType: 'json',
		success: function(response){
			$("#frog_detail").html(response.data);
			$("#action").html(response.action);
		}
	});
}

$(document).on("submit", "#add_form", function (e) {
	e.preventDefault();
    var form = $(this);
    $("#name").parent().removeClass('has-error');
	var name = $("#name").val();
	var gender = $("#gender").val();
	
	if(name == ""){
		$("#name").parent().addClass('has-error');
		return false;
	}
	
	$.ajax({
		url: "ajax.php",
		type: "post",
		data: 'method=add_frog&name='+name+'&gender='+gender,
		dataType: 'json',
		success: function(response){
			if(response.status == "success"){
				get_pond();
				get_pond_data();
			}
		}
	});
	
})
