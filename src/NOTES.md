This is a simple system for managing the frogs.

System functionalities/widgets:
	- pond details (number of frogs, number of female and male frogs)
	- pond (list of frogs)
	- add form (add additional frog in the pond)
	- frog details
		- name 
		- gender 
		- parents
		-offspring
	- action (the action list will show if you click the frog name in the pond)
		- mate (select a frog to mate with)
		- remove in the pond

I used a text files for storing the list of frogs in the pond. Separate text file for storing female and male names (for generating a new frog after mating)

I used bootstrap for the Grid System

NOTE:
please check the read and write permissions of the following files to ensure that the code will work
	- frogs.txt
	- male_names.txt
	- female_names.txt