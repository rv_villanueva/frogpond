<?php
$functions = new functions;
if(isset($_POST['method'])){
	$functions->$_POST['method']($_POST);
}


class functions{
	function get_pond(){
		$list_frogs = file_get_contents('frogs.txt');
		$list_frogs = json_decode($list_frogs,true);
		foreach($list_frogs['frogs'] as $id=>$data){
			echo '<div class="col-md-1"><div style="cursor: pointer" onclick="get_frog_data('.$id.')">'.$data['name'].'</div></div>';
		}
	}
	
	function get_pond_data(){
		$list_frogs = file_get_contents('frogs.txt');
		$list_frogs = json_decode($list_frogs,true);
		$total = 0;
		$female = 0;
		$male = 0;
		foreach($list_frogs['frogs'] as $name=>$data){
			$total = $total+1;
			if($data['gender'] == "female"){
				$female = $female+1;
			}
			if($data['gender'] == "male"){
				$male = $male+1;
			}
		}
		
		$return['total'] = $total;
		$return['female'] = $female;
		$return['male'] = $male;
		
		echo json_encode($return);
	}
	
	function get_frog_data($data){
		$list_frogs = file_get_contents('frogs.txt');
		$list_frogs = json_decode($list_frogs,true);
		
		$response['data'] = 
			'<div class="panel panel-default">
				<div class="panel-heading">Frog detail</div>
				<table class="table table-hover">
					<tbody class="">
						<tr>
							<td>Name</td>
							<td>'.$list_frogs['frogs'][$data['id']]['name'].'</td>
						</tr>
						<tr>
							<td>Gender</td>
							<td>'.ucfirst($list_frogs['frogs'][$data['id']]['gender']).'</td>
						</tr>';
		
		if($list_frogs['frogs'][$data['id']]['parents'] != ""){
			$response['data'].= '<tr>
							<td>Parents</td>
							<td>'.$list_frogs['frogs'][$data['id']]['parents'].'</td>
						</tr>';
		}
		
		if($list_frogs['frogs'][$data['id']]['offspring'] != ""){
			$response['data'].= '<tr>
							<td>Offsprings</td>
							<td>'.$list_frogs['frogs'][$data['id']]['offspring'].'</td>
						</tr>';
		}
		
		$response['data'] .= '</tbody>
				</table>
			</div>';
		
		$options = "";
		foreach($list_frogs['frogs'] as $key=>$frogs){
			if($data['id'] != $key){
				$options .= '<option value='.$key.'>'.$frogs['name'].'</option>';
			}
		}
		
		$response['action'] = 
			'
			<form>
				<div class="panel panel-default">
					<div class="panel-heading">Action</div>
					<table class="table table-hover">
						<tbody class="">
							<tr>
								<td>Remove from Pond</td>
								<td><button onclick="remove_from_pond('.$data['id'].')" type="button" class="btn btn-default btn-xs" autocomplete="off">Go</button></td>
							</tr>
							<tr>
								<td>
									Mate With 
									<select id="mate_with">
										'.$options.'
									</select>
								</td>
								<td><button onclick="mate('.$data['id'].')" type="button" class="btn btn-default btn-xs" autocomplete="off">Go</button></td>
							</tr>
						</tbody>
					</table>
				</div>
			</form>';
		echo json_encode($response);
	}
	
	function add_frog($data){
		$list_frogs = file_get_contents('frogs.txt');
		$list_frogs = json_decode($list_frogs,true);
		
		$list_frogs['frogs'][] = array('name' => $data['name'],'gender' => $data['gender'],'parents' => '','offspring' => '');
		
		file_put_contents('frogs.txt', json_encode($list_frogs));
		
		echo json_encode(array('status' => 'success'));
	}
	
	function mate($data){
		$list_frogs = file_get_contents('frogs.txt');
		$female_names = file_get_contents('female_names.txt');//get the female names
		$male_names = file_get_contents('male_names.txt');//get the male names
		$female_names = json_decode($female_names,true);
		$male_names = json_decode($male_names,true);
		$list_frogs = json_decode($list_frogs,true);//get the existing frogs in pond
		
		$frog_1 = $list_frogs['frogs'][$data['frog_1']];
		$frog_2 = $list_frogs['frogs'][$data['frog_2']];
		
		$gender_list = array('female','male');
		$gender = rand(0,1);//0 = female, 1 = male;
		
		//check if the 2 frogs has the same gender, if not, add new frog in the pond
		if($frog_1['gender'] != $frog_2['gender']){
			//add new frog in the pond
			$rand_id = rand(0,99);
			if($gender == 0 ){
				$name = $female_names[$rand_id];
				unset($female_names[$rand_id]);//to prevent getting the same name, we will remove the "new" name from the list
				$list_frogs['frogs'][] = array('name' => $name,'gender' => 'female','parents' => $frog_1['name'].' and '.$frog_2['name'],'offspring' => '');
			}
			else{
				$name = $male_names[$rand_id];
				unset($male_names[$rand_id]);//to prevent getting the same name, we will remove the "new" name from the list
				$list_frogs['frogs'][] = array('name' => $name,'gender' => 'male','parents' => $frog_1['name'].' and '.$frog_2['name'],'offspring' => '');
			}
			$list_frogs['frogs'][$data['frog_1']]['offspring'] .= $name.'<br/>';
			$list_frogs['frogs'][$data['frog_2']]['offspring'] .= $name.'<br/>';
			file_put_contents('frogs.txt', json_encode($list_frogs));
			echo $frog_1['name'].' successfully mated with '.$frog_2['name'].'. They have a new '.$gender_list[$gender].' child: '.$name;
		}
		else{
			echo $frog_1['name'].' cannot mate with '.$frog_2['name'].' because they are both '.$frog_1['gender'];
		}
	}
	
	function remove_from_pond($data){
		$list_frogs = file_get_contents('frogs.txt');
		$list_frogs = json_decode($list_frogs,true);
		
		echo $list_frogs['frogs'][$data['frog_id']]['name'].' successfully removed from pond';
		unset($list_frogs['frogs'][$data['frog_id']]);
		file_put_contents('frogs.txt', json_encode($list_frogs));
	}
}